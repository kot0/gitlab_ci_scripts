#!/bin/bash

echo "Setup runner for multiarch dind"

echo $CI_RUNNER_TAGS
if [[ "$CI_RUNNER_TAGS" == *"shared"* ]]; then
  echo "SHARED RUNNER"
  docker run --privileged --rm tonistiigi/binfmt --uninstall qemu-*
  docker run --privileged --rm tonistiigi/binfmt --install all
else
  echo "NORMAL RUNNER"
fi

echo "OK"
