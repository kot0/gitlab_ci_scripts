#!/bin/bash

export INSTALL_PATH=/usr/local/bin/s5cmd
export TMP_FOLDER=tmp_s5cmd_install

echo "Installing s5cmd"

set -x

mkdir -p $TMP_FOLDER
cd $TMP_FOLDER || exit 1
curl -o tmp.tar.gz --progress-bar -L "https://github.com/peak/s5cmd/releases/download/v2.2.2/s5cmd_2.2.2_Linux-64bit.tar.gz"
tar -xf tmp.tar.gz
rm -f $INSTALL_PATH
mv s5cmd $INSTALL_PATH
chmod +x $INSTALL_PATH
cd ..
rm -rf $TMP_FOLDER

echo "OK"

# TODO: ARCH DETECTION
