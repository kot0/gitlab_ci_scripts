#!/bin/bash

echo "Setup runner docker buildx"

echo "Creating buildkitd.toml config"
cat <<EOT >>buildkitd.toml
[registry."docker.io"]
mirrors = ["130.61.100.46:5000"]
[registry."130.61.100.46:5000"]
http = true
EOT

set -x
docker buildx create --use --config buildkitd.toml

echo "OK"
